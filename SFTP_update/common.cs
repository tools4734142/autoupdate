﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace TYClientManagerA
{
    public class MWindowObj
    {
        public IntPtr hwnd = IntPtr.Zero;
        public string title = string.Empty;
        public string classname = string.Empty;
        public IntPtr hwndParent = IntPtr.Zero;
    }

    class common
    {
        #region References Windows
        public delegate bool CallBack(IntPtr hwnd, string info);

        [DllImport("user32.dll")]
        public static extern int EnumWindows(CallBack x, int y);

        [DllImport("user32")]
        public static extern IntPtr GetParent(IntPtr hwnd);

        [DllImport("user32")]
        static extern int GetWindowText(IntPtr hwnd, StringBuilder lptrString, int nMaxCount);

        [DllImport("User32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        static extern int GetClassName(IntPtr hwnd, StringBuilder lpClassName, int nMaxCount);

        [DllImport("user32")]
        public static extern int IsWindowVisible(IntPtr hwnd);

        [DllImport("user32")]
        public static extern bool IsWindow(IntPtr hwnd);

        [DllImport("user32.dll", EntryPoint = "FindWindow")]
        public extern static IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", EntryPoint = "FindWindowEx", SetLastError = true)]
        public static extern IntPtr FindWindowEx(IntPtr hwndParent, IntPtr hwndChildAfter, string lpszClass, string lpszWindow);

        [DllImport("user32.dll")]
        public static extern bool SetForegroundWindow(IntPtr hWnd);

        [DllImport("user32.dll")]
        public static extern IntPtr GetForegroundWindow();

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr SetFocus(IntPtr hWnd);

        [DllImportAttribute("user32.dll")]
        public extern static IntPtr GetDC(IntPtr hWnd);

        public struct RECT
        {
            public int Left;                             //最左坐标

            public int Top;                             //最上坐标

            public int Right;                           //最右坐标

            public int Bottom;                        //最下坐标

        }
        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool GetWindowRect(IntPtr hWnd, ref RECT lpRect);
        public static UInt32 _BM_CLICK = 0x00F5;
        #endregion


        #region References Send Message Clipboard
        public struct COPYDATASTRUCT      //this is Window System Message Struct.
        {
            public IntPtr dwData;
            public int cbData;
            [MarshalAs(UnmanagedType.LPStr)]
            public string lpData;
        }
        [DllImport("user32.dll", EntryPoint = "SendMessageA")]
        public static extern int SendMessageA(IntPtr hwnd, int wMsg, IntPtr wParam, ref COPYDATASTRUCT lParam);
        [DllImport("user32.dll", CharSet = CharSet.Auto)]
        public static extern IntPtr SendMessage(IntPtr hWnd, UInt32 msg, IntPtr wParam, IntPtr lParam);
        [DllImport("user32.dll")]
        public static extern IntPtr GetOpenClipboardWindow();
        [DllImport("user32.dll")]
        public static extern IntPtr CloseClipboard();
        #endregion


        #region 枚举窗口信息
        public static List<MWindowObj> result_GetCurAllWindowName = new List<MWindowObj>();
        public static void GetCurAllWindowName()
        {
            result_GetCurAllWindowName.Clear();
            EnumWindows(GetWndAllInformation, 0);
            return;
        }
        public static string GetWindowTitle(IntPtr hwnd)
        {
            StringBuilder sb = new StringBuilder(512);
            GetWindowText(hwnd, sb, sb.Capacity);
            return (sb.Length > 0) ? sb.ToString() : string.Empty;
        }
        public static string GetWindowClassName(IntPtr hwnd)
        {
            StringBuilder sb = new StringBuilder(512);
            GetClassName(hwnd, sb, sb.Capacity);
            return (sb.Length > 0) ? sb.ToString() : string.Empty;
        }
        private static bool GetWndAllInformation(IntPtr hwnd, string info)
        {
            if (IsWindowVisible(hwnd) != 1) return true;
            MWindowObj mWindowObj = new MWindowObj();
            mWindowObj.hwnd = hwnd;
            mWindowObj.title = GetWindowTitle(hwnd);
            mWindowObj.hwndParent = GetParent(hwnd);
            mWindowObj.classname = GetWindowClassName(hwnd);
            result_GetCurAllWindowName.Add(mWindowObj);
            return true;
        }


        /// <summary>
        /// 窗口搜索， Title ClassName 可以为空。

        /// </summary>
        /// <param name="sTitle"></param>
        /// <param name="sClassName"></param>        
        /// <returns></returns>
        public static MWindowObj GetWindowsHandleFromEnum(string sTitle, string sClassName)
        {
            if ((sTitle == "") && (sClassName == "")) return null;
            GetCurAllWindowName();
            foreach (MWindowObj v in result_GetCurAllWindowName)
            {
                if (v.classname.Contains(sClassName) && v.title.Contains(sTitle))
                {
                    return v;
                }
            }
            return null;
        }

        public static IntPtr GetWindowsHandleFromEnum(string sTitle, string sClassName, IntPtr lastHWND)
        {
            if (lastHWND != IntPtr.Zero)
            {
                if (IsWindow(lastHWND))
                    return lastHWND;
            }
            GetCurAllWindowName();
            foreach (MWindowObj v in result_GetCurAllWindowName)
            {
                if (v.classname.Contains(sClassName) && v.title.Contains(sTitle))
                {
                    return v.hwnd;
                }
            }
            return IntPtr.Zero;
        }
        #endregion


        #region Send msg to TY_application
        const int WM_COPYDATA = 0x004A;   //this is Window System Message Type.
        public static void SendTYWndMsg(string str, string strWndTitle = "TYPay AutoPress Tool")
        {
            IntPtr hW = FindWindow(null, strWndTitle);
            if (hW == IntPtr.Zero) return;
            string msg = str + "\0";
            var cds = new COPYDATASTRUCT
            {
                dwData = new IntPtr(3),
                cbData = msg.Length + 1,
                lpData = msg
            };
            SendMessageA(hW, WM_COPYDATA, IntPtr.Zero, ref cds);
        }
        #endregion


        #region Computer
        //-- Drives枚举
        public static string GetAllDriveInfo()
        {
            string result = string.Empty;
            DriveInfo[] allDrives = DriveInfo.GetDrives();
            foreach (DriveInfo d in allDrives)
            {
                result += (string.Format("Drive {0}, File type: {1}, Volume label: {2}",
                    d.Name, d.DriveType, d.VolumeLabel)) + "\r\n";
            }
            return result;
        }
        //-- 进程枚举
        public static string GetCurProcessName()
        {
            string result = string.Empty;
            Process[] myProcesses = Process.GetProcesses();
            foreach (Process myProcess in myProcesses)
            {
                result += (myProcess.Id + ", " + myProcess.ProcessName + ", " +
                    myProcess.MainWindowTitle) + "\r\n";
            }
            return result;
        }
        public static string GetMyWLANIPAddressB()
        {
            HttpClient httpClient = new HttpClient();
            try
            {
                //var returnCitySN = {"cip": "203.90.239.2", "cid": "810000", "cname": "香港特别行政区"};
                HttpResponseMessage response = httpClient.GetAsync("http://pv.sohu.com/cityjson").Result;
                if (response.IsSuccessStatusCode)
                {
                    Task<string> t = response.Content.ReadAsStringAsync();
                    string s = t.Result;
                    s = s.Replace("var returnCitySN = ", "").Replace(";", "");
                    var htResult = JsonConvert.DeserializeObject<Hashtable>(s);
                    if (htResult.Contains("cip"))
                        return htResult["cip"] as string;
                }
            }
            catch
            { }
            return string.Empty;
        }
        #endregion


        #region XML Config
        private ArrayList GetXMLInformation(string xmlFilePath, string itemName = "", string subItemName = "")
        {
            ArrayList resultList = new ArrayList();
            try
            {
                //初始化一个xml实例
                XmlDocument myXmlDoc = new XmlDocument();
                //加载xml文件（参数为xml文件的路径）
                myXmlDoc.Load(xmlFilePath);
                //获得第一个姓名匹配的节点（SelectSingleNode）：此xml文件的根节点
                XmlNode rootNode = myXmlDoc.SelectSingleNode("configuration");
                //获得该节点的子节点（即：该节点的第一层子节点）

                XmlNodeList firstLevelNodeList = rootNode.ChildNodes;
                foreach (XmlNode node in firstLevelNodeList)
                {
                    string name = node.Name;
                    string txt = node.InnerText;
                    if ((subItemName == "") && (name.IndexOf(itemName) >= 0))
                    {
                        resultList.Add(txt);
                    }
                    //判断此节点是否还有子节点
                    if (node.ChildNodes.Count > 1)
                    {
                        for (int i = 0; i < node.ChildNodes.Count; i++)
                        {
                            XmlNode xn1 = node.ChildNodes[i];
                            string name1 = xn1.Name;
                            string txt1 = xn1.InnerText;

                            if ((name1.IndexOf(subItemName) >= 0) && (name.IndexOf(itemName) >= 0))
                            {
                                resultList.Add(txt1);
                            }
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                //log(ex.ToString());
                return null;
            }
            return resultList;
        }
        //配置文件操作
        public static void ModifyItem(string keyName, string newKeyValue)
        {
            System.Configuration.Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            config.AppSettings.Settings[keyName].Value = newKeyValue;
            config.Save(ConfigurationSaveMode.Modified);
            ConfigurationManager.RefreshSection("appSettings");

        }
        #endregion


        #region 模拟键盘输入
        public static void WinInput(string Keys, int cleanChars = 11, int nDelay = 6)
        {
            //SendKeys.Send("{END}");
            for (int i = 0; i < cleanChars; i++)
                SendKeys.SendWait(@"{BACKSPACE}");
            for (int i = 0; i < cleanChars; i++)
                SendKeys.SendWait(@"{DELETE}");
            SetString(Keys, nDelay);
        }
        public static void SetString(string str, int nDelay = 6)
        {
            SendKeys.SendWait("");
            foreach (char c in str)
            {
                SendKeys.SendWait(c.ToString());
                if (nDelay > 0)
                    Thread.Sleep(nDelay);
            }
        }

        [DllImport("user32.dll", SetLastError = true)]
        public static extern void keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo); //dwFlags,     //这里为整数类型 0为按下，2为释放


        #endregion


        #region Helper
        /// <summary>
        /// 获取北京时间
        /// </summary>
        public static DateTime GetBeijingTime()
        {
            DateTime dt = DateTime.Now;
            try
            {
                //发送请求的数据
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://a.jd.com//ajax/queryServerData.html");
                request.Method = "GET";
                //发送成功后接收返回的XML信息
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Encoding enc = Encoding.GetEncoding("UTF-8");
                Stream streamRead = response.GetResponseStream();
                StreamReader streamReader = new StreamReader(streamRead, enc);
                string html = streamReader.ReadToEnd();
                if (html.Length > 0)
                {
                    Hashtable time = JsonConvert.DeserializeObject<Hashtable>(html);
                    //dt = ConvertStringToDateTime(time["serverTime"]);
                    //log.Info("北京时间:" + dt.ToLocalTime().ToString());
                    return dt;
                }
            }
            catch (WebException e)
            {
                return DateTime.Now;
            }
            catch (Exception)
            {
                return DateTime.Now;
            }
            return dt;
        }
        //--检查并创建目录
        // sFilename: @"D:/tes/asdfasdf/asdf/asdf/t/"
        // sFilename: @"D:\\tes\\asdfasdf\\"
        public static void CheckDir(String sFilename)
        {
            sFilename = sFilename.Replace("\\\\", "/");
            if (!sFilename.Contains("/")) return;
            sFilename = sFilename.Substring(0, sFilename.LastIndexOf("/"));
            string[] str1 = sFilename.Split('/');
            string str2 = string.Empty;
            for (int i = 0; i < str1.Count(); i++)
            {
                string v = str1[i];
                if (v.Length == 0) continue;
                if (v.Contains(":")) continue;
                str2 += v + "/";
                FileInfo fi = new FileInfo(str2);
                var di = fi.Directory;
                if (di == null) continue;
                if (!di.Exists)
                    di.Create();
            }
        }

        //得到当前时间戳(毫秒)
        public static long GetNowTimeStamp()
        {
            System.DateTime startTime = TimeZone.CurrentTimeZone.ToLocalTime(new System.DateTime(1970, 1, 1)); // 当地时区
            long timeStamp = (long)(DateTime.Now - startTime).TotalMilliseconds; // 相差毫秒数

            return timeStamp;
        }

        public static void SetListViewInit(ListView ti)
        {
            ti.FullRowSelect = true;
            //ti.CheckBoxes = false;
            ti.GridLines = true;
            ti.MultiSelect = true;
            ti.BackColor = Color.LightYellow; //LightSteelBlue, YellowGreen LightSalmon, LightYellow,
            ti.View = View.Details;
            //ti.Columns.Clear();
            ti.Items.Clear();
        }

        /// <summary>
        /// 16进制UTF8的字符串 转 明文
        /// </summary>
        /// <param name="strHex"></param>
        /// <returns></returns>
        public static string ConvertHEXUTF8StrToData(string strHex)
        {
            int size = strHex.Length / 2;
            byte[] arr1 = new byte[size];
            for (int i = 0; i < size; i++)
            {
                string s1 = strHex.Substring(i * 2, 2);
                int t1 = Convert.ToInt32(s1, 16);
                arr1[i] = (byte)t1;
            }
            string s = Encoding.UTF8.GetString(arr1);
            return s;
        }
        public static string ConvertDataToHEXUTF8Str(string strData)
        {
            byte[] bs = Encoding.UTF8.GetBytes(strData);
            string result = "";
            foreach (byte b in bs)
            {
                result += (b.ToString("x2").ToUpper());
            }
            return (result);
        }
        /// <summary>
        ///将字符串写入文件。

        /// </summary>
        /// <param name="filePath">文件的绝对路径</param>
        /// <param name="text">字符串数据</param>
        /// <param name="encoding">字符编码</param>
        public static bool WriteFile_text(string filePath, string text, Encoding encoding)
        {
            try
            {
                CheckDir(filePath);
                //如果文件不存在则创建该文件

                if (!System.IO.File.Exists(filePath))
                {
                    //创建文件
                    FileInfo file = new FileInfo(filePath);
                    using (FileStream stream = file.Create())
                    {
                        using (StreamWriter writer = new StreamWriter(stream, encoding))
                        {
                            //写入字符串     
                            writer.Write(text);
                            //输出
                            writer.Flush();
                        }
                    }
                }
                else
                {
                    using (StreamWriter writer = new StreamWriter(filePath))
                    {
                        writer.WriteLine(text);
                    }
                }
                return true;
            }
            catch
            {
                return false;
            }
        }
        public static bool WriteFile_text_UTF8(string filePath, string text)
        {
            return WriteFile_text(filePath, text, Encoding.UTF8);
        }
        #endregion


        #region 进程互斥
        [DllImport("User32.dll")]
        private static extern bool ShowWindowAsync(IntPtr hWnd, int cmdShow);
        public static Process GetRunningInstance()
        {
            Process currentProcess = Process.GetCurrentProcess(); //获取当前进程
                                                                  //获取当前运行程序完全限定名 
            string currentFileName = currentProcess.MainModule.FileName;
            //获取进程名为ProcessName的Process数组。 
            Process[] processes = Process.GetProcessesByName(currentProcess.ProcessName);
            //遍历有相同进程名称正在运行的进程 
            foreach (Process process in processes)
            {
                if (process.MainModule.FileName == currentFileName)
                {
                    if (process.Id != currentProcess.Id) //根据进程ID排除当前进程 
                        return process;//返回已运行的进程实例 
                }
            }
            return null;
        }
        private const int WS_SHOWNORMAL = 1;
        public static bool HandleRunningInstance(Process instance)
        {
            //确保窗口没有被最小化或最大化 
            ShowWindowAsync(instance.MainWindowHandle, WS_SHOWNORMAL);
            //设置为foreground window 
            return SetForegroundWindow(instance.MainWindowHandle);
        }
        public static bool HandleRunningInstance()
        {
            Process p = GetRunningInstance();
            if (p != null)
            {
                HandleRunningInstance(p);
                return true;
            }
            return false;
        }

        public static void ExecuteAsAdmin(string rootPath, string fileName)
        {
            if (fileName == "") return;
            FileInfo ti = new FileInfo(rootPath + "/" + fileName);
            if (!ti.Exists)
            {
                MessageBox.Show("文件不存在:" + fileName, "错误", MessageBoxButtons.OK);
                return;
            }
            Process proc = new Process();
            proc.StartInfo.FileName = fileName;
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.WorkingDirectory = rootPath + "/" + fileName.Split('/')[0];
            proc.StartInfo.Verb = "runas";
            proc.Start();
        }
        public static void ExecuteAsAdmin(string fileName)
        {
            if (fileName == "") return;
            FileInfo ti = new FileInfo(fileName);
            if (!ti.Exists)
            {
                MessageBox.Show("文件不存在:" + fileName, "错误", MessageBoxButtons.OK);
                return;
            }
            Process proc = new Process();
            proc.StartInfo.FileName = fileName;
            proc.StartInfo.UseShellExecute = true;
            proc.StartInfo.Verb = "runas";
            proc.Start();
        }
        public static bool CheckExe(string processName)
        {
            //获得进程对象，以用来操作
            System.Diagnostics.Process myproc = new System.Diagnostics.Process();
            //得到所有打开的进程    
            try
            {

                //获得需要杀死的进程名   
                foreach (Process thisproc in Process.GetProcessesByName(processName))
                {
                    return true;
                }
            }
            catch (Exception Exc)
            {
                throw new Exception("", Exc);
            }
            return false;
        }
        public static void KillExe(string moduleName)
        {
            System.Diagnostics.Process myproc = new System.Diagnostics.Process();
            try
            {
                foreach (Process thisproc in Process.GetProcessesByName(moduleName))
                    thisproc.Kill();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
        #endregion


        #region 内存
        //定义内存的信息结构

        [StructLayout(LayoutKind.Sequential)]
        public struct MEMORY_INFO
        {
            public uint dwLength;
            public uint dwMemoryLoad;
            public uint dwTotalPhys;
            public uint dwAvailPhys;
            public uint dwTotalPageFile;
            public uint dwAvailPageFile;
            public uint dwTotalVirtual;
            public uint dwAvailVirtual;
        }
        [DllImport("kernel32")]
        private static extern void GetWindowsDirectory(StringBuilder WinDir, int count);
        [DllImport("kernel32")]
        private static extern void GetSystemDirectory(StringBuilder SysDir, int count);
        [DllImport("kernel32")]
        private static extern void GlobalMemoryStatus(ref MEMORY_INFO meminfo);

        /// <summary>
        /// 打印内存信息
        /// </summary>
        public static void PrintMemInfo()
        {
            //Console.WriteLine(GetMemInfo());
        }

        /// <summary>
        /// 获取内存信息
        /// </summary>
        /// <returns></returns>
        public static string GetMemInfo()
        {
            //调用GlobalMemoryStatus函数获取内存的相关信息

            MEMORY_INFO MemInfo = new MEMORY_INFO();
            GlobalMemoryStatus(ref MemInfo);
            return MemInfo.dwMemoryLoad.ToString();

            //StringBuilder sb = new StringBuilder();
            ////*%的内存正在使用

            //sb.Append(MemInfo.dwMemoryLoad.ToString() + "% of the memory is being used " + "\r\n");
            ////总共的物理内存

            //sb.Append("Physical memory total :" + Utility.ConvertBytes(MemInfo.dwTotalPhys.ToString(), 3) + "GB" + "\r\n");
            ////可使用的物理内存
            //sb.Append("Use of physical memory :" + Utility.ConvertBytes(MemInfo.dwAvailPhys.ToString(), 3) + "GB" + "\r\n");
            ////交换文件总大小

            //sb.Append("Total size of the swap file" + Utility.ConvertBytes(MemInfo.dwTotalPageFile.ToString(), 3) + "GB" + "\r\n");
            ////尚可交换文件大小为

            //sb.Append(" Can still swap file size :" + Utility.ConvertBytes(MemInfo.dwAvailPageFile.ToString(), 3) + "GB" + "\r\n");
            ////总虚拟内存

            //sb.Append("The Total virtual memory :" + Utility.ConvertBytes(MemInfo.dwTotalVirtual.ToString(), 3) + "GB" + "\r\n");
            ////未用虚拟内存有

            //sb.Append("Unused virtual memory :" + Utility.ConvertBytes(MemInfo.dwAvailVirtual.ToString(), 3) + "GB" + "\r\n");
            //// ConvertBytes(totMem, 3) + " GB"
            //return sb.ToString();
        }
        public static string getCPUCounter()
        {
            PerformanceCounter cpuCounter = new PerformanceCounter();
            cpuCounter.CategoryName = "Processor";
            cpuCounter.CounterName = "% Processor Time";
            cpuCounter.InstanceName = "_Total";
            // will always start at 0 
            dynamic firstValue = cpuCounter.NextValue();
            System.Threading.Thread.Sleep(1000);
            // now matches task manager reading 
            dynamic secondValue = cpuCounter.NextValue();
            return Convert.ToInt32(secondValue).ToString();
        }
        #endregion



        /// <summary>
        /// 默认密钥-密钥的长度必须是32
        /// </summary>
        private const string PublicKey = "616263646566676a6b6b656463777378";

        /// <summary>
        /// 默认向量
        /// </summary>
        private const string AesIv = "666768696a6b6c6d6e6f706162636465";
        /// <summary>  
        /// AES加密  
        /// </summary>  
        /// <param name="str">需要加密字符串</param>  
        /// <returns>加密后字符串</returns>  
        public static String Encrypt(string str)
        {
            return Encrypt(str, ConvertHEXUTF8StrToData(PublicKey), ConvertHEXUTF8StrToData(AesIv));
        }

        /// <summary>  
        /// AES解密  
        /// </summary>  
        /// <param name="str">需要解密字符串</param>  
        /// <returns>解密后字符串</returns>  
        public static String Decrypt(string str)
        {
            return Decrypt(str, ConvertHEXUTF8StrToData(PublicKey), ConvertHEXUTF8StrToData(AesIv));
        }
        /// <summary>
        /// AES加密
        /// </summary>
        /// <param name="str">需要加密的字符串</param>
        /// <param name="key">32位密钥</param>
        /// <returns>加密后的字符串</returns>
        public static string Encrypt(string str, string key, string iv)
        {
            Byte[] keyArray = System.Text.Encoding.UTF8.GetBytes(key);
            Byte[] toEncryptArray = System.Text.Encoding.UTF8.GetBytes(str);
            var rijndael = new System.Security.Cryptography.RijndaelManaged();
            rijndael.Key = keyArray;
            rijndael.Mode = System.Security.Cryptography.CipherMode.ECB;
            rijndael.Padding = System.Security.Cryptography.PaddingMode.PKCS7;
            rijndael.IV = System.Text.Encoding.UTF8.GetBytes(iv);
            System.Security.Cryptography.ICryptoTransform cTransform = rijndael.CreateEncryptor();
            Byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
            return Convert.ToBase64String(resultArray, 0, resultArray.Length);
        }
        /// <summary>
        /// AES解密
        /// </summary>
        /// <param name="str">需要解密的字符串</param>
        /// <param name="key">32位密钥</param>
        /// <returns>解密后的字符串</returns>
        public static string Decrypt(string str, string key, string iv)
        {
            try
            {
                Byte[] keyArray = System.Text.Encoding.UTF8.GetBytes(key);
                Byte[] toEncryptArray = Convert.FromBase64String(str);
                var rijndael = new System.Security.Cryptography.RijndaelManaged();
                rijndael.Key = keyArray;
                rijndael.Mode = System.Security.Cryptography.CipherMode.ECB;
                rijndael.Padding = System.Security.Cryptography.PaddingMode.PKCS7;
                rijndael.IV = System.Text.Encoding.UTF8.GetBytes(iv);
                System.Security.Cryptography.ICryptoTransform cTransform = rijndael.CreateDecryptor();
                Byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                return System.Text.Encoding.UTF8.GetString(resultArray);
            }
            catch
            {
                return "";
            }
        }
    }
}
