﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Runtime.InteropServices;
using System.Xml;
using System.Collections;
using System.Configuration;
using System.Threading;
using System.Diagnostics;
using System.Net;
using Terminal_CDMA;
using System.Reflection;
using TYClientManagerA;

namespace WindowMoniter
{
     
    public partial class FormSFTP : Form
    {
        string strIgnore = ConfigurationManager.AppSettings["ignore"];
         
        public FormSFTP()
        {
            InitializeComponent();

            richTextBoxLog.Clear();            
            progressBar1.Value = 0;
            label1.Text = "";
            label1.Text = ("版本检查....");
            label1.BackColor = Color.Transparent;            
            this.Text = "自动更新--" + Assembly.GetEntryAssembly().GetName().Version.ToString();
            //if (common.HandleRunningInstance() == false)
            //{
            //    button2.Visible = !checkUpdate();
            //}
            //else
            //    this.Close();

            
        }
        private void FormSFTP_Load(object sender, EventArgs e)
        {
            if (bool.Parse(ConfigurationManager.AppSettings["IsAutoRun"]))
            {
                sftp_Update();
            }
        }

        #region Log
        public void saveLog()
        {
            common.CheckDir(System.Environment.CurrentDirectory + "/log/");
            DateTime dt = DateTime.Now;
            string logFile = dt.ToString("yyyyMMddHHmmss");
            richTextBoxLog.SaveFile($"{System.Environment.CurrentDirectory}/log/{logFile}_Launcher.txt", RichTextBoxStreamType.PlainText);
        }        
        public void tlog(String str)
        {
            DateTime dt = DateTime.Now;
            richTextBoxLog.AppendText($"[{dt.ToLongTimeString()}]{str}\r\n");
            richTextBoxLog.ScrollToCaret();
            if(richTextBoxLog.Lines.Count()>9999)
            {
                //saveLog();
                richTextBoxLog.Clear();
            }
        }
        #endregion


        #region SFTP--更新\下载 
        List<string> gDownList = new List<string>();

        private bool func_isNeedDown(MFileInfo v, string root)
        {
            if (v.IsDirectory) return false;
            FileInfo ti = new FileInfo(Environment.CurrentDirectory + v.fullname.Replace(root, "/"));
            if (ti.Exists)
            {
                if ((v.filesize != ti.Length.ToString()) || (ti.LastWriteTime < DateTime.Parse(v.lastWriteTime)))
                {
                    return true;
                }
            }
            else
            {
                common.CheckDir(System.Environment.CurrentDirectory + v.fullname.Replace(root, "/"));
                return true;
            }
            return false;
        }
        private bool _func_checkUpdate(SFTPHelper ftp, string dir, string root)
        {
            try
            {
                ArrayList res = ftp.GetFileList(dir);
                foreach (MFileInfo v in res)
                {
                    if (!v.IsDirectory)
                    {
                        // file
                        if (func_isNeedDown(v, root))
                        {
                            string filename = v.fullname.Replace(root, "/");
                            //filter some
                            if (strIgnore.Contains($"|{v.name}|"))
                            {
                                tlog($"Ignore file: {v.name}");
                                continue;
                            }
                            tlog($"Add file: {filename}");
                            gDownList.Add(filename);
                        }                        
                    }
                    else
                    {
                        // directory
                        common.CheckDir(System.Environment.CurrentDirectory + v.fullname.Replace(root, "/") + "/");
                        if (strIgnore.Contains($"|{v.name}|"))
                        {
                            tlog($"Ignore directory: {v.name}");
                            continue;
                        }
                        _func_checkUpdate(ftp, v.fullname, root);
                    }
                }
                return true;
            }
            catch (Exception e1)
            {
                tlog(e1.Message);
            }
            return false ;
        }
        
        private void sftp_Update()
        {
            label1.Text = ("版本检查....");
            try
            {
                string ftpHost = ConfigurationManager.AppSettings["ftp_IP"];
                string ftp_Port = ConfigurationManager.AppSettings["ftp_Port"];
                string ftp_user = ConfigurationManager.AppSettings["ftp_user"];
                string ftp_pin = ConfigurationManager.AppSettings["ftp_pin"];
                string ftp_dir = ConfigurationManager.AppSettings["ftp_dir"];
                ftp_user = (common.Decrypt(common.ConvertHEXUTF8StrToData(ftp_user)));
                ftp_pin = (common.Decrypt(common.ConvertHEXUTF8StrToData(ftp_pin)));

                SFTPHelper ftp = new SFTPHelper(ftpHost, ftp_Port, ftp_user, ftp_pin);
                bool isResult = _func_checkUpdate(ftp, ftp_dir, ftp_dir);
                if (isResult)
                {
                    progressBar1.Maximum = gDownList.Count;
                    progressBar1.Value = 0;
                    foreach (string v in gDownList)
                    {
                        progressBar1.Value++;
                        //tlog(v.filename + ", " + v.filesize + ", " + v.lastWriteTime); 
                        tlog("正在下载: " + v);
                        string localPath = System.Environment.CurrentDirectory + v;
                        ftp.Get(ftp_dir + v.Substring(1), localPath);
                    }
                    label1.Text = ("已经是最新版本");
                    tlog("已经是最新版本，可以关闭了。");
                    button1.Visible = false;
                    
                    progressBar1.Value = progressBar1.Maximum;
                }                
            }
            catch (Exception e1)
            {
                label1.Text = (e1.Message);                
            }
        }
        #endregion


        #region  辅助
        private void func_deleteFiles()
        {
            string needDeleteFiles = ConfigurationManager.AppSettings["oldfiles"];
            string[] files = needDeleteFiles.Split(',');
            foreach (string path in files)
            {
                string localPathVer = System.Environment.CurrentDirectory + "/" + path;
                FileInfo ti1 = new FileInfo(localPathVer);
                if (ti1.Exists)
                {
                    try
                    {
                        tlog($"删除文件: {localPathVer}");
                        ti1.Delete();
                    }
                    catch { }
                }
            }
        }
        private void func_runMainExe()
        {
            string ProName = ConfigurationManager.AppSettings["ClientPath"];
            ProName = ProName.Replace(".exe", "").Replace("./", "");
            if (common.CheckExe(ProName))
            {
                if (MessageBox.Show("自动机正在运行,强行关闭吗？", "警告", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                {
                    return;
                }
                else
                {
                    common.KillExe(ProName);
                }
            }
            tlog("Startup: " + System.Environment.CurrentDirectory + "/" + ConfigurationManager.AppSettings["ClientPath"]);
            common.ExecuteAsAdmin(System.Environment.CurrentDirectory + "/" + ConfigurationManager.AppSettings["ClientPath"]);
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            sftp_Update();
            func_deleteFiles();
            //Close();
            button1.Enabled = true;
        }

        #endregion

        
    }


}
