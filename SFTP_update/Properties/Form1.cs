﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using TYComm;
using static TYComm.winring0;


//<!-- Press功能： 拉盾、拉按盾、协助按盾、协助输入密码、协助启动MainProcess -->
namespace TyPay
{
    public partial class Form1 : Form
    {
        private static log4net.ILog log = log4net.LogManager.GetLogger("Form1");
        CheckUKey checkUKey = new CheckUKey();  //
        string gInputPWD = string.Empty;        //密码输入协助
        public static string gPressName = string.Empty; //和机器名绑定的按盾器名称
        long nTickLastHelp = ComputerInfo.GetNowTimeStamp();    //最后一次协助操作时间

        #region FormOnEvent
        public Form1()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            if (ProcesHelper.HandleRunningInstance())
            {
                ProcesHelper.KillSelfProcess();
                Application.Exit();
            }

            richTextBoxLog.Clear();
            gPressName = GetPressName();
            AutoClick.CreateObjCOM(comboBox1);
            timer1.Interval = 2000;
            timer1.Enabled = true;
            checkUKey.Init();
            this.Text = "TYPay AutoPress Tool";
        }
        private void Form1_Load(object sender, EventArgs e)
        {
        }
        private void Form1_Shown(object sender, EventArgs e)
        {
            this.Left = 0;
            this.Width = 271;
            this.Height = 136;
            this.Top = 768 - this.Height-35;
            //this.Opacity = 0.6;
        }
        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                tlog("Application Closed.");
                AutoClick.OnApplicationExit(sender, e);
                saveLog();
            }
            catch
            {            }
        }
         
        private void Button2_Click_1(object sender, EventArgs e)
        {
            if (button2.Text == "EN")
            {
                button2.Text = "CN";                
                button1.Text = "Init";
                button3.Text = "Press";
            }
            else
            {
                button2.Text = "EN";
                button1.Text = "初始化";
                button3.Text = "按盾";
            }
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            AutoClick.CreateObjCOM(comboBox1);
            checkUKey.ResetChecking();
        }
        private void Button3_Click(object sender, EventArgs e)
        {
            AutoPress();
        }
        #endregion


        #region Wnd_Message
        private void GetOrderFromAPP(string s)
        {
            tlog(s);
            if(s.Contains("TYPayAutoPress"))
            {
                AutoPress();
            }
            if(s.Contains("TYClosePress"))
            {
                Form1_FormClosed(null, null);
                Application.Exit();
                return;
            }
            if ((s.Contains("TYInputPWD"))&&(s.Contains(":")))
            {
                //命令格式： TYInputPWD:QQ123123
                string[] tmp = s.Split(':');
                gInputPWD = tmp[1].Trim();
            }
        }
        const int WM_COPYDATA = 0x004A;   //this is Window System Message Type.
        public struct COPYDATASTRUCT      //this is Window System Message Struct.
        {
            public IntPtr dwData;
            public int cbData;
            [MarshalAs(UnmanagedType.LPStr)]
            public string lpData;
        }
        protected override void DefWndProc(ref Message m)
        {
            try
            {
                switch (m.Msg)
                {
                    case WM_COPYDATA:
                        COPYDATASTRUCT cds = new COPYDATASTRUCT();
                        Type t = cds.GetType();
                        cds = (COPYDATASTRUCT)m.GetLParam(t);
                        string strResult = cds.lpData.ToString();
                        GetOrderFromAPP(strResult);
                        break;
                    default:
                        base.DefWndProc(ref m);
                        break;
                }
            }
            catch
            {            }
        }
        #endregion


        #region Log.; 需要控件： richTextBoxLog       
        public void saveLog()
        {
            //-- 删除3天前的Log
            try
            {
                List<string> list1 = new List<string>();
                DirFileStream.DoSearchFiles(new DirectoryInfo(System.Environment.CurrentDirectory + "/log/"), list1);
                for (int i = 0; i < list1.Count; i++)
                {
                    string s = list1[i].ToString();
                    FileInfo fi = new FileInfo($"{s}");
                    if (fi.LastWriteTime.AddDays(3) > DateTime.Now) continue;
                    System.IO.File.Delete(list1[i].ToString());
                }
                list1.Clear();
                DirFileStream.DoSearchFiles(new DirectoryInfo(System.Environment.CurrentDirectory + "/App_Data/"), list1);
                for (int i = 0; i < list1.Count; i++)
                {
                    string s = list1[i].ToString();
                    FileInfo fi = new FileInfo($"{s}");
                    if (fi.LastWriteTime.AddDays(3) > DateTime.Now) continue;
                    System.IO.File.Delete(list1[i].ToString());
                }
            }
            catch
            { }         
        }
        public void tlog(string str)
        {
            log.Info(str);
            richTextBoxLog.AppendText($"{DateTime.Now.ToString("HH:mm:ss")}> {str}\r\n");
            richTextBoxLog.ScrollToCaret();
            if(richTextBoxLog.Lines.Length > 9999)
            {
                saveLog();
                richTextBoxLog.Clear();
            }
        }
        #endregion


        #region 协助输入密码
        private bool func_WndInputUKeyPWD(IntPtr hExp)
        {
            IntPtr hCur = WindowUtils.GetForegroundWindow();
            if (hCur != hExp)
            {
                WindowUtils.SetForegroundWindow(hExp);
            }            
            {
                winring0.init();
                List<Key> list = winring0.SetKey(gInputPWD);
                foreach (Key k in list)
                {
                    winring0.KeyDown(k);
                    Thread.Sleep(106);
                    winring0.KeyUp(k);
                }
                Thread.Sleep(100);
                winring0.KeyDown(winring0.Key.VK_RETURN);
                Thread.Sleep(100);
                winring0.KeyUp(winring0.Key.VK_RETURN);                
                return true;
            }
        }
        private bool func_WndAutoInputPWD()
        {
            long nt = (long)(ComputerInfo.GetNowTimeStamp() - nTickLastHelp);
            if (Math.Abs(nt) < 20000) return false;

            WindowUtils.GetCurAllWindowName();
            for (int i = 0; i < WindowUtils.result_GetCurAllWindowName.Count; i++)
            {
                MWindowObj mWindowObj = WindowUtils.result_GetCurAllWindowName[i];

                // except Title, continue;
                if (ClassFunc.CheckStringContainKeys(ConfigUtils.GetValue("Except_AutoPressTitle").Split(','), mWindowObj.title))
                    continue;

                string configClassname = ConfigUtils.GetValue("Bank_UKeyClass");
                if (!mWindowObj.classname.Contains(configClassname))
                    continue;

                if (ClassFunc.CheckStringContainKeys(ConfigUtils.GetValue("Bank_UKeyTitle").Split(','), mWindowObj.title))
                {
                    nTickLastHelp = ComputerInfo.GetNowTimeStamp();
                    timer1.Enabled = false;
                    tlog($"{mWindowObj.title}, {mWindowObj.classname}");
                    tlog("Input PWD OK");
                    func_WndInputUKeyPWD(mWindowObj.hwnd);
                    return true;
                }
            }
            return false;
        }
        #endregion


        #region 监控主Client
        private bool func_MoniteMainClient()
        {
            if (!bool.Parse(ConfigUtils.GetValue("IsMoniteClient"))) return false;
            long nt = (long)(ComputerInfo.GetNowTimeStamp() - nTickLastHelp);
            if (Math.Abs(nt) < 20000) return false;

            string processName = ConfigUtils.GetValue("ExeFilename");
            if (!ProcesHelper.FindPrecess(processName.Replace(".exe", "")))
            {
                tlog($"启动: {processName}");
                ProcesHelper.ExecuteAsAdmin($"{Environment.CurrentDirectory}/{processName}");
                nTickLastHelp = ComputerInfo.GetNowTimeStamp();
                return true;
            }
            return false;
        }
        #endregion


        #region Logic Funtion
        //根据机器名得到Press名字，验证机器名合法性, 返回按盾器名字，否则为空
        private string GetPressName()
        {
            if (!bool.Parse(ConfigUtils.GetValue("IsLockPress"))) return string.Empty;

            string idxComputer = string.Empty;
            string computerName = Environment.MachineName;
            if (computerName.Contains("-"))
            {
                idxComputer = computerName.Substring(computerName.LastIndexOf("-") + 1);
                //验证命名合法性
                try
                {
                    int idx = int.Parse(idxComputer);
                    tlog($"机器编号： {idx}");
                    string result = $"press_{idxComputer}";
                    tlog($"将锁定按盾器: {result}");
                    return result;
                }
                catch {
                    tlog("机器名规则不对。正确是： 部门-组-编号");
                }                
            }
            return string.Empty;
        }
        //按盾
        private void AutoPress()
        {
            tlog("Press UKey");
            AutoClick.CreateObjCOM(comboBox1);
            AutoClick.Press(1);
        }

        //协助按盾        
        private bool func_WndAutoPress()
        {
            long nt = (long)(ComputerInfo.GetNowTimeStamp() - nTickLastHelp);
            if (Math.Abs(nt) < 20000) return false;
            

            WindowUtils.GetCurAllWindowName();
            for (int i = 0; i < WindowUtils.result_GetCurAllWindowName.Count; i++)
            {
                MWindowObj mWindowObj = WindowUtils.result_GetCurAllWindowName[i];

                // except Title, continue;
                if (ClassFunc.CheckStringContainKeys(ConfigUtils.GetValue("Except_AutoPressTitle").Split(','), mWindowObj.title))
                    continue;

                string configClassname = ConfigUtils.GetValue("Bank_AutoPressClass");
                if (!mWindowObj.classname.Contains(configClassname))
                    continue;

                if (ClassFunc.CheckStringContainKeys(ConfigUtils.GetValue("Bank_AutoPressTitle").Split(','), mWindowObj.title)||
                    (mWindowObj.title ==string.Empty))
                {
                    nTickLastHelp = ComputerInfo.GetNowTimeStamp();
                    tlog($"{mWindowObj.title}, {mWindowObj.classname}");
                    tlog("AutoClick OK");
                    AutoPress();
                    return true;
                }
            }
            return false;
        }
        #endregion


        


        #region Controller
        private void Timer1_Tick(object sender, EventArgs e)
        {
            if (gPressName != string.Empty) label2.Text = gPressName;

            //Help Press
            if (bool.Parse(ConfigUtils.GetValue("IsHelpPress")))
            {
                if (func_WndAutoPress())
                    return;
            }

            //Help InputPWD
            if (bool.Parse(ConfigUtils.GetValue("IsHelpInputPWD")) && (gInputPWD != string.Empty))
            {
                if (func_WndAutoInputPWD())
                {
                    timer1.Enabled = true;
                    return;
                }
            }

            //IsMoniteClient
            if (bool.Parse(ConfigUtils.GetValue("IsMoniteClient")))
            {
                if (func_MoniteMainClient())
                    return;
            } 

            //monite UKey&Press
            string strOut = "";
            if (checkUKey.IsChecking(comboBox1, progressBar1, out strOut))
            {
                tlog(strOut);
                if (strOut.Contains("按盾器初始化OK"))
                {
                    AutoPress();
                }
                return;
            }
            if(strOut!=string.Empty)
                tlog(strOut);

            //--
            
        }
        #endregion

    }

}