﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.IO;
using System.Collections;
using System.Configuration;
using System.Net;
using System.Reflection;
using UpdateFTP;
using TYClientManagerA;
using static System.Net.WebRequestMethods;

namespace WindowMoniter
{
     
    public partial class FormFTP : Form
    {
        List<MMFileInfo> mMFileInfos = new List<MMFileInfo>();

        string[] gIgnoreNames = ConfigurationManager.AppSettings["ignore"].Split('|');

        public FormFTP()
        {
            InitializeComponent();

            richTextBoxLog.Clear();            
            progressBar1.Value = 0;
            label1.Text = "";
            label1.Text = ("版本检查....");
            label1.BackColor = Color.Transparent;            
            this.Text = "自动更新--" + Assembly.GetEntryAssembly().GetName().Version.ToString();

        }

        private void FormFTP_Load(object sender, EventArgs e)
        {
            if (bool.Parse(ConfigurationManager.AppSettings["IsAutoRun"]))
            {
                Button1_Click(sender, e);
            }
        }
        private bool func_isExistIgnore(string fullFileName)
        {
            foreach(string v in gIgnoreNames)
            {
                if(string.IsNullOrEmpty(v)) continue;
                if(fullFileName.EndsWith(v)) return true;
            }
            return false;   
        }


        #region Log           
        public void tlog(String str)
        {
            DateTime dt = DateTime.Now;
            richTextBoxLog.AppendText($"[{dt.ToLongTimeString()}]{str}\r\n");
            richTextBoxLog.ScrollToCaret();
            if(richTextBoxLog.Lines.Count()>9999)
            {
                richTextBoxLog.Clear();
            }
        }
        #endregion


        #region FTP--更新\下载
        List<string> gDownList = new List<string>();
        private void func_deleteFiles()
        {
            string needDeleteFiles = ConfigurationManager.AppSettings["oldfiles"];
            string[] files = needDeleteFiles.Split(',');
            foreach (string path in files)
            {
                if (string.IsNullOrEmpty(path)) continue;
                string filename1 = System.Environment.CurrentDirectory + "/" + path;
                if (System.IO.File.Exists(filename1))
                {
                    try
                    {
                        tlog($"删除文件: {filename1}");
                        System.IO.File.Delete(filename1);
                    }
                    catch { }
                }
            }
        }
        private bool func_isNeedDown(string fullname, string filesize, string root)
        {
            FileInfo ti = new FileInfo(Environment.CurrentDirectory + fullname.Replace(root, ""));
            if (ti.Exists)
            {
                if ((filesize == ti.Length.ToString()) )
                {
                    return false;
                }
            }
            return true;
        }

        private bool _func_loopAllFiles(FtpClient ftpClient, string dir)
        {
            try
            {
                string[] strings = ftpClient.directoryListDetailed(dir);
                foreach (string v in strings)
                {
                    if (string.IsNullOrEmpty(v)) continue;
                    string v1 = v.Replace("  ", " ");
                    string[] strings1 = v1.Split(' ');
                    if (strings1.Count() < 6) break;
                    bool isDir = false;
                    if (v.Contains("drwxr")) isDir = true;
                    string filename = strings1[strings1.Count() - 1];
                    if (filename == ".") continue;
                    if (filename == "..") continue;
                    if (func_isExistIgnore(filename)) continue;
                    string filesize = strings1[strings1.Count() - 5];
                    MMFileInfo mMFile = new MMFileInfo();
                    mMFile.Name = $"{dir}/{filename}";
                    mMFile.isDir = isDir;
                    mMFile.filesize = filesize;                    
                    mMFileInfos.Add(mMFile);

                    //tlog($"{mMFile.Name}; {filesize}; {isDir}");

                    if (isDir)
                    {
                        _func_loopAllFiles(ftpClient, mMFile.Name);
                    }
                }
                return true;
            }
            catch (Exception e1)
            {
                tlog(e1.Message);
            }
            return false;
        }
        private void func_checkFiles(string root)
        {
            foreach(MMFileInfo v in mMFileInfos)
            {
                if (v.isDir)
                {
                    common.CheckDir(System.Environment.CurrentDirectory + v.Name.Replace(root, "") + "/");
                }
                else
                {
                    if(func_isNeedDown(v.Name, v.filesize, root))
                    {
                        string filename1 = v.Name.Replace(root, "");
                        //tlog($"-> {filename1}");
                        gDownList.Add(filename1);
                    }
                }
            }
        }
        private bool func_ftpUpdate()
        {
            label1.Text = ("版本检查....");
            try
            {
                string ftpHost = ConfigurationManager.AppSettings["ftp_IP"];
                string ftp_Port = ConfigurationManager.AppSettings["ftp_Port"];
                string ftp_user = ConfigurationManager.AppSettings["ftp_user"];
                string ftp_pin = ConfigurationManager.AppSettings["ftp_pin"];
                string ftp_dir = ConfigurationManager.AppSettings["ftp_dir"];
                FtpClient ftpClient = new FtpClient($"ftp://{ftpHost}:{ftp_Port}", ftp_user, ftp_pin);
                _func_loopAllFiles(ftpClient, ftp_dir);
                if (mMFileInfos.Count<=0) {
                    tlog("错误： 链接失败");
                    return false; 
                }
                func_checkFiles(ftp_dir);
                if (gDownList.Count > 0)
                {
                    progressBar1.Maximum = gDownList.Count;
                    progressBar1.Value = 0;
                    foreach (string v in gDownList)
                    {
                        progressBar1.Value++;
                        tlog("正在下载: " + v);
                        string localPath = System.Environment.CurrentDirectory + v;
                        ftpClient.download(ftp_dir + v, localPath);
                    }                    
                    progressBar1.Value = progressBar1.Maximum;
                }
                label1.Text = ("更新完毕。");
                tlog("更新完毕。");
                return true;
            }
            catch (Exception e1)
            {
                label1.Text = (e1.Message);
            }
            return false;
        }        
        #endregion

         
        private void Button1_Click(object sender, EventArgs e)
        {
            button1.Enabled = false;
            func_deleteFiles();
            if (func_ftpUpdate())
            {                
                //Close();
            }            
            button1.Enabled = true;
        }
         
    }

    public class MMFileInfo
    {
        public string Name { get; set; }
        public bool isDir { get; set; }
        public string filesize { get; set; }
    }

}
